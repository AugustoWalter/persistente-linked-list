package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;
import static org.junit.jupiter.api.Assumptions.*;

public class PersistenteLinkedListTest {

	private static final String  TAB_NAME = "lista1";

	private static final Long ID_LISTA = 100L;

	private static PersistentList<String> nomes;

	@BeforeEach
	public void setup() throws SQLException {
		assumeTrue(DbUtil.isConnectionValid());
		nomes = new PersistenteLinkedList<>();
		limparListaBanco();


	}

	@AfterAll
	public static void teardown() throws SQLException {
		assumeTrue(DbUtil.isConnectionValid());
		limparListaBanco();  //limpa sujeira do banco
	}

	@Test
	@DisplayName("Teste persistência de uma lista com 4 elementos")
	public void testPersist4Elements() throws InvalidElementException, SQLException, IOException, ClassNotFoundException {
		// Dados de entrada
		// Execução do método a ser testado


		nomes.add("antonio"); 
		nomes.add("claudio");  //defeito ativado, levando ao erro.
		nomes.add("pedreira");
		nomes.add("neiva");


		nomes.persist(ID_LISTA, DbUtil.getConnection(), TAB_NAME);
		nomes.clear();
		nomes.load(ID_LISTA, DbUtil.getConnection(), TAB_NAME); //traz dados do banco

		// Saída esperada
		// Verificação se a saída esperada é igual à saída atual

		Assertions.assertAll("Verificar add", // teste feito na list local
				() ->Assertions.assertEquals(4, nomes.size()),
				() ->Assertions.assertEquals("antonio", nomes.get(0)),
				() ->Assertions.assertEquals("claudio", nomes.get(1), "O nome na posição 1 deveria ser claudio"),
				() ->Assertions.assertEquals("pedreira", nomes.get(2)),
				() ->Assertions.assertEquals("neiva", nomes.get(3)),
				() ->Assertions.assertNull(nomes.get(10)));



	}

	private static void limparListaBanco() throws SQLException {
		nomes.delete(ID_LISTA, DbUtil.getConnection(), TAB_NAME);
	}




}
