package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;


public class LinkedListParametrizadoTest {

	private List<String> nomes;

	@BeforeEach //antes de cada método de teste da classe, isso aqui será executado, recriando a lista.
	public void setup() throws InvalidElementException  {
		nomes = new LinkedList<>();
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("pedreira");
		nomes.add("neiva");


	}
	//	@ParameterizedTest(name = "{index} get({0}) = {1}")
	//	@MethodSource("fornecerDadosTestGet") 
	//	public void testGet(Integer i, String nomeEsperado) throws InvalidElementException {
	//		Assertions.assertEquals(nomeEsperado, nomes.get(i));
	//	}
	//
	//
	//	public static Stream<Arguments> fornecerDadosTestGet(){ // recomendando usar quando a carga dos dados envolve instanciação de uma classe, situação mais complexa.
	//		return Stream.of(
	//				Arguments.of(0, "antonio"),
	//				Arguments.of(1, "claudio"),
	//				Arguments.of(2, "pedreira"),
	//				Arguments.of(3, "neiva"));
	//
	//
	//	};
	@ParameterizedTest(name = "{index} get({0}) = {1}")
	@CsvSource({"0, antonio", "1, claudio", "2, pedreira", "3, neiva"}) // recomendado para dados primitivos 
	public void testGet(Integer i, String nomeEsperado) throws InvalidElementException {
		Assertions.assertEquals(nomeEsperado, nomes.get(i));
	}

	@Test
	public void testGetNull() {
		Assertions.assertNull(nomes.get(10));
	}
	@Test
	public void testClear() {
		Integer qtdEsperada = 0;
		nomes.clear();
		Integer qtdAtual = nomes.size();
		Assertions.assertEquals(qtdEsperada, qtdAtual);

	}

}
