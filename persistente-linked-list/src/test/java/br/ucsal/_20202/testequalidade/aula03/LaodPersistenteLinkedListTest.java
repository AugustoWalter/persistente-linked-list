package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public class LaodPersistenteLinkedListTest {

	private static final String  TAB_NAME = "lista1";

	private static final Long ID_LISTA = 100L;

	private static PersistentList<String> nomes;

	@BeforeEach
	public void setup() throws SQLException, InvalidElementException, IOException {
		assumeTrue(DbUtil.isConnectionValid());
		nomes = new PersistenteLinkedList<>();
		limparListaBanco(); 
		nomes.add("antonio"); 
		nomes.add("claudio");  //defeito ativado, levando ao erro.
		nomes.add("pedreira");
		nomes.add("neiva");
		nomes.persist(ID_LISTA, DbUtil.getConnection(), TAB_NAME);
		nomes.clear();


	}

	@AfterAll
	public static void teardown() throws SQLException {
		assumeTrue(DbUtil.isConnectionValid());
		limparListaBanco();  //limpa sujeira do banco
	}

	@Test
	@DisplayName("Teste persistência de uma lista com 4 elementos")
	public void testLoad4Elements() throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		nomes.load(ID_LISTA, DbUtil.getConnection(), TAB_NAME); //traz dados do banco

		Assertions.assertAll("Verificar add", // teste feito na list local
				() ->Assertions.assertEquals(4, nomes.size()),
				() ->Assertions.assertEquals("antonio", nomes.get(0)),
				() ->Assertions.assertEquals("claudio", nomes.get(1), "O nome na posição 1 deveria ser claudio"),
				() ->Assertions.assertEquals("pedreira", nomes.get(2)),
				() ->Assertions.assertEquals("neiva", nomes.get(3)),
				() ->Assertions.assertNull(nomes.get(10)));

	}

	private static void limparListaBanco() throws SQLException {
		nomes.delete(ID_LISTA, DbUtil.getConnection(), TAB_NAME);
	}







}
