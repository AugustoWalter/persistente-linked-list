package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

public class LinkedListTest {
	private List<String> nomes;

	@BeforeEach
	public void setup() {

		nomes = new LinkedList<>();

	}


	@Test
	@DisplayName("Teste de inclusão de 4 elementos")
	public void testAdd4Elements() throws InvalidElementException {
		// Dados de entrada
		// Execução do método a ser testado

		nomes.add("antonio"); 
		nomes.add("claudio");  //defeito ativado, levando ao erro.
		nomes.add("pedreira");
		nomes.add("neiva");

		// Saída esperada
		// Verificação se a saída esperada é igual à saída atual

		Assertions.assertAll("Verificar add",
				() ->Assertions.assertEquals("antonio", nomes.get(0)),
				() ->Assertions.assertEquals("claudio", nomes.get(1), "O nome na posição 1 deveria ser claudio"),  // falha ativada e reconhecidada!
				() ->Assertions.assertEquals("pedreira", nomes.get(2)),
				() ->Assertions.assertEquals("neiva", nomes.get(3)));


	}

	@Test
	public void testSize3Elements() throws InvalidElementException {
		// Dados de entrada
		// Execução do método a ser testado
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("pedreira");

		// Saída esperada
		// Verificação se a saída esperada é igual à saída atual
		Assertions.assertEquals(3, nomes.size());

	}
	@Test
	public void testInvalidElement() throws InvalidElementException{
		InvalidElementException exception = Assertions.assertThrows(InvalidElementException.class, () -> nomes.add(null));
		Assertions.assertEquals("The element can't be null.", exception.getMessage());


	}

}
